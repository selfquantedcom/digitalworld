Rails.application.routes.draw do

  #configure devise 
  devise_scope :user do
   post '/' => "users/sessions#login"
   get '/' => "users/sessions#login"
   post "/login" => "users/sessions#create"
   get "/login" => "users/sessions#create"
   post '/register' => 'users/registrations#create'
   get '/register' => 'users/registrations#create'

   # Overwrite existing device login form
   get '/users/sign_up' => 'users/sessions#register'
   get '/users/sign_in' => 'users/sessions#login'
   
   delete '/sign_out' => 'users/sessions#destroy'            
 end

 devise_for :users, controllers: { sessions: 'users/sessions' }
 root 'users/sessions#login'

 # Navigation links
 post '/tutorials' => "users/search#home"
 get '/tutorials' => "users/search#home"

 post '/search' => "users/search#home"
 get '/search' => "users/search#home"

 post '/test_page' => "users/test#home"
 get '/test_page' => "users/test#home"

 # Articles 
 post '/articles/article1' => "articles/articles#article1"
 get '/articles/article1' => "articles/articles#article1"

 post '/articles/article2' => "articles/articles#article2"
 get '/articles/article2' => "articles/articles#article2"
end
