class Users::SearchController < ApplicationController   
   before_action :authenticate_user!

   def home
      # @developer_key = ENV['GOOGLE_PICKER_DEVELOPER_KEY']
      # @client_id = ENV['GOOGLE_PICKER_CLIENT_ID']
      # @app_id = ENV['APPLICAITON_ID']
      render template: "users/search"      
   end

end