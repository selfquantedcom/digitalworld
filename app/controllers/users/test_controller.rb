class Users::TestController < ApplicationController
   # THIS IS A SESSION RESTRICTED AREA 
   before_action :authenticate_user!

   def home
      render template: "users/test_page"
      
   end

end